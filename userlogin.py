


class UserLogin:
    def formdb(self, userid, db):
        self.__user = db.getuser(userid)
        return self
    
    def create(self, user):
        self.__user = user
        return self
    
    def is_authenticated(self):
        return True
    
    def is_active(self):
        return True
    
    def anounymous(self):
        return False
    
    def get_id(self):
        return str(self.__user['id'])
    
