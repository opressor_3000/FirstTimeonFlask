from flask import Flask, render_template, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from datetime import datetime
from flask_login import LoginManager, login_required
from werkzeug.security import generate_password_hash, check_password_hash
from forms import LoginForm, BookForm, Registrate
from userlogin import UserLogin

app = Flask(__name__) # создаем приложение как объект Flask

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://Yusif:admin12345@127.0.0.1:3306/bookdb'
app.config['SQlALCHEMY_TRACK_MODIFICATIONS'] = False

login_manager = LoginManager(app) # создаем объект класса авторизации --------------- <<<<<<<<<<<<<<<<

db = SQLAlchemy(app) 
db.create_all() # создает базу данных и таблицы 

migrate = Migrate(app, db)  #  создаем объект миграции


# flask db init                     # нужно сделать так что миграции проходили через кастомную функцию 
# flask db migrate -m 'comments'
# flask db upgrade


#--------------------------------------------------------------------------------------------------------------------------------------------

#############               Модели для создания таблиц в БД              ################

class Book(db.Model): 
   id = db.Column(db.Integer, primary_key = True)
   title = db.Column(db.String(50), unique=True)
   price = db.Column(db.Float, nullable=True)
   past_price = db.Column(db.Float)
   discription = db.Column(db.String(500), nullable=True)
   image = db.Column(db.LargeBinary)
   quantity = db.Column(db.Integer, nullable=True)

   lang = db.relationship('Book', backref='lang', uselist=False)
   gen = db.relationship('Book', backref='genre', uselist=False)
   auth = db.relationship('Book', backref='author', uselist=False)
   pub = db.relationship('Book', backref='publisher', uselist=False)
   stock = db.relationship('Book', backref='stock', uselist=False)
   user = db.relationship('User', backref='user', uselist=False)

   def __repr(self):
      return '< Book %r>' 

class Lang(db.Model):
   id = db.Column(db.Integer, primary_key = True)
   language = db.Column(db.String(100), unique=True, nullable=True)

   def __repr(self):
      return '<Language: %r>' 

class Genre(db.Model):
   id = db.Column(db.Integer, primary_key = True)
   genre = db.Column(db.String(100), unique=True, nullable=True)

   def __repr(self):
      return '<Genre: %r>' 
   
class Author(db.Model):
   id = db.Column(db.Integer, primary_key = True)
   author = db.Column(db.String(100), unique=True, nullable=True)

   def __repr(self):
      return '<Author %r>'

class Publisher(db.Model):
   id = db.Column(db.Integer, primary_key = True)
   publisher = db.Column(db.String(100), unique=True, nullable=True)

   def __repr(self):
      return '< Publisher: %r>'
   
class Stoke(db.Model):
   id = db.Column(db.Integer, primary_key = True)
   stock = db.Column(db.String(100), unique=True, nullable=True)
   address = db.Column(db.String(100), unique=True, nullable=True)

   def __repr(self):
      return '< Publisher: %r>'
   
class User(db.Model):
   id = db.Column(db.Integer, primary_key = True)
   email = db.Column(db.String(100), unique=True, nullable=True)
   password = db.Column(db.String(100), nullable=True)
   username = db.Column(db.String(100), unique=True, nullable=True)
   create = db.Column(db.DateTime, nullable=True)
   

# -------------------------------------------------------------------------------------------------------------------------------------------

# Классы с методами выборки из БазыДанных -----------------<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
class FDataBase:  
   def __init__(self) -> None:
      self.db = db

   def sessiondb(self, db):
      pass
    
   def getuser(self, userid):
      try:
         self
      except:
         pass

# --------------------------------------------------------------------------------------------------------------------------------------------


@app.route('/', methods = ['GET', 'POST'])   #  add BOOK to DataBase
@login_required   # ограничивает доступ для не авторизованных пользователей 
def index(): #    страница для обавления записей в БД через WTForm --------
   book = BookForm
   if book.validate_on_submit():
      try:
         
         pass
      except:
         print('add book error')
   return render_template('index.html', title='add book')

@app.route('/product', methods = ['GET'])  # SELECT all BOOK 
def product(): # отображение всех данных из БД

   return render_template('index.html', title='Products')

@app.errorhandler(404)
def pagenotfound(error): # перехват ошибки
   return render_template('page404.html', title='Page not found'), 404

@app.route('/login', methods = ['GET', 'POST'])  # 
def login():
   form = LoginForm()
   user = db.session.get(User, form.email.data)   
   if form.validate_on_sibmit():
      if user and check_password_hash(user['']): # Проверяем существ такой email и верный ли пароль в БД

         remainme = form.remember.data
      # user = User.query.filter(email[]).all() 
      userlogin = UserLogin().create(user)  # сюда надо передать всю информацию по user из БД для авторизации 
   return render_template('index.html', title='username')

@app.route('/registr', methods = ['GET', 'POST']) # регистрации через валидацию 
def registr(): #              add USER --------------<<<<<<<<<<<<<<<<<<<<<<<<<<<<
   reg = Registrate()
   if not User.query.filter_by(email = request.form['email']).all() and reg.validate_on_submit():
      hash = generate_password_hash(request.form['pass'])
      
      db.session.add()
      db.session.commit()
   return redirect(url_for('login'))


if __name__ == '__main__':
   app.run(debug=True)


'''
      var = Modelname.query.all()
      var[index].columnname
      var2 = Modelname.query.first()
      var2.colimnname
      Modelname.query.filter_by(columnname = value).all()
      Modelname.query.filter(columnname == value).all()
      Modelname.query.limit(3).all()
      Modelname.query.order_by(Modelname.columnname.desc()).all()
      Modelname.query.get(valprimarykey)
'''
 
