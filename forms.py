from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, BooleanField, SearchField, PasswordField, DecimalField, TextAreaField, IntegerField, MultipleFileField
from wtforms.validators import DataRequired, Email, Length

class Registrate:
    name = StringField('Username', validators=[DataRequired, Length(min=6, max=50)])
    email = StringField('Email: ', validators=[Email(message='Not a valid email address')])
    psw = PasswordField('Password: ', validators=[DataRequired(), Length(min=6, max=100)])
    pswagain = PasswordField('Password: ', validators=[DataRequired(), Length(min=6, max=100)])
    submit = SubmitField('authorized')


class LoginForm:
    email = StringField('Email: ', validators=[Email(message='Not a valid email address')])
    password = PasswordField('Password: ', validators=[DataRequired(), Length(min=6, max=100)])
    remember = BooleanField('Remainme', default=False)
    submit = SubmitField('LogIn')

class BookForm:
    title = StringField('Title', validators=[DataRequired()])
    price = DecimalField('Price', validators=[Length(min=1)])
    lang = StringField('Lang', validators=[]) # добавить валидатор который будет принимать если введенные значения совпадают с выборкой из БД (список)
    gen = StringField('Genree', validators=[]) # 
    auth = StringField('Author', validators=[]) #
    pub = StringField('Publisher', validators=[])#
    stock = StringField('Stock', validators=[DataRequired]) 
    disc = TextAreaField('Discription', validators=[DataRequired(), Length(min=20, max=1000)])
    quant = IntegerField('Quantity', validators=[Length(min=1)])
    image = MultipleFileField('Image')
    submit = SubmitField('AddBook')

